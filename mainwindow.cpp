#include "mainwindow.hpp"
#include "ui_mainwindow.h"
#include <QDoubleSpinBox>
#include<iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->eksp_but,&QPushButton::clicked,this,&MainWindow::changeExpFile);
    connect(ui->theory_but,&QPushButton::clicked,this,&MainWindow::changeTheoryDir);
    connect(ui->eksp_file,&QLineEdit::textChanged,this,&MainWindow::ekspChanged);
    connect(ui->theory_dir,&QLineEdit::textChanged,this,&MainWindow::theoryChanged);
    connect(ui->left_box,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this,&MainWindow::updateLeft);
    connect(ui->right_box,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this,&MainWindow::updateRight);
    connect(ui->left_max,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this,&MainWindow::updateLeft);
    connect(ui->right_max,static_cast<void(QDoubleSpinBox::*)(double)>(&QDoubleSpinBox::valueChanged),this,&MainWindow::updateRight);
    connect(ui->swap_box,&QCheckBox::toggled,this,&MainWindow::updateSwap);
    reg=QRegularExpression("Rabi=\\d+\\.\\d+");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeExpFile(){
    auto file=QFileDialog::getOpenFileName(this,"Izvelies eksperimentu failu");
    if(!file.isEmpty()){
        ui->eksp_file->setText(file);
    }
}

void MainWindow::changeTheoryDir(){
    auto dir=QFileDialog::getExistingDirectory(this,"Izvelies direktoriju");
    if(!dir.isEmpty()){
        ui->theory_dir->setText(dir);
    }
}
void MainWindow::ekspChanged(){
    QFileInfo fileIn(ui->eksp_file->text());
    if(fileIn.exists()){
        QFile file(fileIn.absoluteFilePath());
        if(!file.open(QIODevice::ReadOnly))
            throw std::runtime_error("Nespeju atvert eksperimenta failu");
        DataObj eksp;
        QTextStream stream(&file);
        QString line;
        while(!(line=stream.readLine()).isNull()){
            QStringList tmp=line.split(" ");
            if(tmp.size()<3)
                throw std::runtime_error("Eksperimenta fails ir kludains");
            eksp.x.push_back(tmp[0].toDouble());
            eksp.left.push_back(tmp[1].toDouble());
            eksp.right.push_back(tmp[2].toDouble());
        }
        eksp_obj=eksp;
    }
    updateLeft(0);
    updateRight(0);
}

void MainWindow::LoadTheoryFile(const QFileInfo & fileIn){
    QFile file(fileIn.absoluteFilePath());

    if(!file.open(QIODevice::ReadOnly)){
        throw std::runtime_error("Nespeju atvert teorijas failu");
    }

    QTextStream stream(&file);
    QString line;
    DataObj theory;
    while(!(line=stream.readLine()).isNull()){
        QStringList tmp=line.split(" ");
        if(tmp.size()<4)
            throw std::runtime_error("Teorijas fails ir kludains");
        theory.x.push_back(tmp[0].toDouble());
        theory.left.push_back(tmp[2].toDouble());
        theory.right.push_back(tmp[3].toDouble());
    }
    QString name=fileIn.fileName();
    QRegularExpressionMatch match = reg.match(name);
    if(!match.hasMatch()){
        throw std::runtime_error("Nezinu Rabi frekvenci "+ name.toStdString());
    }
    theory.label=match.captured(0).replace(tr("Rabi="),tr(""));
    theories.push_back(theory);
    updateLeft(0);
    updateRight(0);
}

void MainWindow::theoryChanged(){
    theories.clear();
    try{
        QDir dir(ui->theory_dir->text());
        if(dir.exists()){
            for(QFileInfo & file:dir.entryInfoList()){
                if(file.isFile()){
                    LoadTheoryFile(file);
                }
            }
        }
    }catch(std::runtime_error & e){
        QMessageBox::warning(this,"Theorijas ielade neizdevas",e.what());
    }

}

void MainWindow::updateLeft(double){
    QCustomPlot * plot=ui->plot_left;
    plot->clearPlottables();
    if(eksp_obj.x.size()!=0){
        auto graph=plot->addGraph();
        graph->setLineStyle(QCPGraph::lsNone);
        graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,Qt::black,Qt::black,7));
        auto is_checked=ui->swap_box->isChecked();
        double max=(is_checked?ui->right_max:ui->left_max)->text().toDouble();
        double backgr=ui->left_box->text().toDouble();
        graph->addData(eksp_obj.x,Normalize(RemoveBackground(is_checked? eksp_obj.right:eksp_obj.left,backgr),max-backgr));
    }
    int t_ind=0;
    for(auto &t:theories){
        auto graph=plot->addGraph();
        QPen pen(QColor::fromHsv(240.0/theories.size()*t_ind,255,255));
        graph->setPen(pen);
        graph->addData(t.x,Normalize(t.left));
        graph->setName(t.label);
        t_ind++;
    }
    plot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignLeft);
    plot->legend->setVisible(true);
    plot->rescaleAxes();
    plot->replot();
}

void MainWindow::updateRight(double){
   QCustomPlot * plot=ui->plot_right;
   plot->clearPlottables();
   if(eksp_obj.x.size()!=0){
       auto graph=plot->addGraph();
       graph->setLineStyle(QCPGraph::lsNone);
       graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle,Qt::black,Qt::black,7));
       auto is_checked=ui->swap_box->isChecked();
       double max=(is_checked?ui->left_max:ui->right_max)->text().toDouble();
       double backgr=ui->right_box->text().toDouble();
       graph->addData(eksp_obj.x,Normalize(RemoveBackground(is_checked? eksp_obj.left:eksp_obj.right,backgr),max-backgr));
   }
   int t_ind=0;
   for(auto &t:theories){
       auto graph=plot->addGraph();
       QPen pen(QColor::fromHsv(240.0/theories.size()*t_ind,255,255));
       graph->setPen(pen);
       graph->addData(t.x,Normalize(t.right));
       graph->setName(t.label);
       t_ind++;
   }
   plot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignLeft);
   plot->legend->setVisible(true);
   plot->rescaleAxes();
   plot->replot();
}
void MainWindow::updateSwap(){
    updateLeft(0);
    updateRight(0);
}
