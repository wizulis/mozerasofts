#ifndef DATA_UTIL_HPP
#define DATA_UTIL_HPP

#include <QVector>
#include <QString>
struct DataObj{
    QString label;
    QVector<double> x,left,right;
};
inline double GetMax(const QVector<double> & data){
    if(data.size()==0){
        return 0;
    }
    double max=data[0];
    for(auto & i:data)
        if(i>max)
            max=i;
    return max;
}

inline QVector<double> Normalize(const QVector<double> & data,double max){
    if(data.size()==0){
        return QVector<double>();
    }
    QVector<double> ret(data);
    for(double & i:ret)
        i/=max;
    return ret;
}

inline QVector<double> Normalize(const QVector<double> & data){
    return Normalize(data,GetMax(data));
    }

inline QVector<double> RemoveBackground(const QVector<double> & data,double backgr){
    QVector<double> ret(data);
    for(auto & r:ret){
        r-=backgr;
    }
    return ret;
}

#endif
