#-------------------------------------------------
#
# Project created by QtCreator 2014-04-29T20:11:14
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
QMAKE_CXXFLAGS += -std=c++11
TARGET = MozeraSofts
TEMPLATE = app

LIBS += -lqcustomplot
SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.hpp \
    data_util.hpp

FORMS    += mainwindow.ui
