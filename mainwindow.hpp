#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QFileInfo>
#include "data_util.hpp"
#include <QRegularExpression>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    QRegularExpression reg;
    Q_OBJECT
    DataObj eksp_obj;
    QVector<DataObj> theories;

    void LoadTheoryFile(const QFileInfo &);
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void changeExpFile();
    void changeTheoryDir();
    void ekspChanged();
    void theoryChanged();
    void updateLeft( double);
    void updateRight(double );
    void updateSwap();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_HPP
